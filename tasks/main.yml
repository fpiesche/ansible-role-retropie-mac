---
- name: Install required packages
  package:
    name: "{{ package }}"
  loop: "{{ required_packages }}"
  loop_control:
    loop_var: package
  become: yes

- name: Clone macemu git repository
  git:
    repo: "{{ macemu_git_url }}"
    dest: "{{ git_dest }}"

- name: Generate configuration for {{ emulator }}
  shell: "NO_CONFIGURE=1 {{ macemu_dest }}/autogen.sh"
  args:
    chdir: "{{ macemu_dest }}"

- name: Configure Basilisk II for compilation
  shell: "{{ macemu_dest }}/configure --enable-sdl-video --enable-sdl-audio --disable-vosf --disable-jit-compiler --without-gtk"
  args:
    chdir: "{{ macemu_dest }}"
  when: emulator == "BasiliskII"

- name: Configure SheepShaver for compilation
  shell: "{{ macemu_dest }}/configure --enable-sdl-video --enable-sdl-audio --disable-vosf --disable-jit --enable-addressing=direct"
  args:
    chdir: "{{ macemu_dest }}"
  when: emulator == "SheepShaver"

- name: Build {{ emulator }}
  make:
    chdir: "{{ macemu_dest }}"

- name: Strip {{ emulator }} executable
  shell: "strip {{ emulator }}"
  args:
    chdir: "{{ macemu_dest }}"

- name: Install {{ emulator }}
  make:
    chdir: "{{ macemu_dest }}"
    target: install
  become: yes

- name: Remove emulator source
  file:
    path: "{{ git_dest }}"
    state: absent

- name: Create directory for Mac disk files
  file:
    path: "{{ mac_rom_dest }}"
    state: directory

- name: Copy MacOS installation image
  copy:
    src: "{{ macos_iso }}"
    dest: "{{ mac_rom_dest }}/"
    mode: 0555

- name: Copy Mac firmware image
  copy:
    src: "{{ mac_rom_file }}"
    dest: "{{ mac_rom_dest }}/mac.rom"
    mode: 0555

- name: Copy hard drive images
  unarchive:
    src: "{{ image }}"
    dest: "{{ mac_rom_dest }}/"
  loop:
    - files/system.img.zip
    - files/8gb.sparsebundle.zip
  loop_control:
    loop_var: image

- name: Copy Basilisk II settings file
  template:
    src: files/basilisk_ii_prefs.j2
    dest: "{{ basilisk_ii_prefs }}"

- name: Copy SheepShaver settings file
  template:
    src: files/sheepshaver_prefs.j2
    dest: "{{ sheepshaver_prefs }}"

- name: Adjust Linux memory map permissions
  sysctl:
    name: vm.mmap_min_addr
    value: '0'
  become: yes

- name: Remove existing RetroPie autostart
  file:
    path: "{{ retropie_autostart }}"
    state: absent

- name: Set {{ emulator }} to autostart
  lineinfile:
    path: "{{ retropie_autostart }}"
    line: "{{ emulator }}"
    create: yes
    mode: 0744

- name: Reboot
  reboot:
  become: yes
